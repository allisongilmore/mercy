# **Tasks in this notebook**
# 1. Make indicator variables for missingness in each column of meta-data.
# 2. Make count of procedures, count of diagnoses in each row.
# 3. Make indicator for count of procedures being 0, count of diagnoses being 0.

# FIX to read whitespace as na if this is not default. 
# distinguish populated from no data.

import pandas as pd
import numpy as np
# import matplotlib
# import matplotlib.pyplot as plt
# from IPython.display import display
# %matplotlib inline
# pd.set_option('display.mpl_style', 'default')

# consider usecols arg to avoid reading unnecessary columns

# to run locally with full file
# df = pd.io.parsers.read_table(
#     "/Users/Allison.Gilmore/Documents/mercy/data/HB UB CLAIM DATA.dsv",
#      sep="|",
#      header=0,
#      index_col=False,
#      na_values=['NO DATA', ' ',''],
#      error_bad_lines=False,
#      warn_bad_lines=True
#      )
# 
# to run on amber
df = pd.io.parsers.read_table(
    "/home/ayasdi/jeanezra/mercy/cleanedlines/ub_correct.dsv",
     sep="|",
     header=0,
     index_col=False,
     na_values=['NO DATA', ' ',''],
     error_bad_lines=False,
     warn_bad_lines=True
     )

# local test run
# df = pd.io.parsers.read_table(
#     "/Users/Allison.Gilmore/Documents/mercy/data/mercy_small.dsv",
#      sep="|",
#      header=0,
#      index_col=False,
#      na_values=['NO DATA', ' ',''],
#      nrows=100)
     
orig_col_len = len(df.columns)
output = "There are %d columns in the input file.\n\n\n" % orig_col_len 

# Make a dictionary of claim form line to dataset column.

# Use string search to make the dictionary for claim lines above 10.
claim_to_col = {}
for n in range(10,82):
    claim_to_col[n] = [c for c in df.columns if c.startswith(str(n))]

# Update by hand for claim lines 1 to 9.
claim_to_col.update({1: [df.columns[0]],
                    2: [df.columns[1]],
                    3: [c for c in df.columns[2:4]],
                    4: [df.columns[4]],
                    5: [df.columns[5]],
                    6: [c for c in df.columns[6:8]],
                    7: [df.columns[8]],
                    8: [c for c in df.columns[9:11]],
                    9: [c for c in df.columns[11:17]],
                    })
output += "The claim-to-column dictionary is: \n %r \n\n\n" % claim_to_col

# Find the columns that should have individual missingness indicators.
indiv = {}
for k in claim_to_col:
    if len(claim_to_col[k]) == 1:
        indiv.update({k: claim_to_col[k]})

# The length == 1 test works except for the condition codes in claim form items 18-28.
# Remove those from this dictionary and handle them below.
for k in range(18,29):
    indiv.pop(k)

output += """Individual indicators will be made for the following columns, 
				unless they are uniformly null: \n %r \n\n\n""" % indiv

# Find the columns that should have group level missingness indicators.
groups = {c:claim_to_col[c] for c in claim_to_col if c not in indiv}

# Fix the condition codes in claim form items 18-28 to be a single group.
groups.update({18: [i for k in range(18,29) for i in claim_to_col[k]]})
for k in range(19,29):
    groups.pop(k)

# groups

# Find (manually) the groups of columns whose missingness indicator should be 1 iff all columns are null.
# This involves combining some groups.
groups_all = {3: groups[3],
            8: groups[8],
            9: groups[9],
            18: groups[18],
            31: groups[31] + groups[32] + groups[33] + groups[34] + groups[35] + groups[36],
            39: groups[39] + groups[40] + groups[41],
            67: groups[67],
            70: groups[70],
            72: groups[72],
            74: groups[74]}

output += """For-all group level indicators will be made for the following columns, 
			: \n %r \n\n\n""" %  groups_all

# Find (manually) the groups of columns whose missingness indicator should be 1 iff any of the columns are null.
groups_any = {6: groups[6],
              76: groups[76]
              }


output += """For-any group level indicators will be made for the following columns, 
			: \n %r \n\n\n""" %  groups_any

# Find (manually) the groups of columns whose missingness indicator should be 1 iff any but not all of the columns are null.

groups_any_not_all = {}

# First, locate the central table columns (claim fields 42 - 48).
# These need a different claim form to column mapping.

central_table = {'42_'+str(n): [c for c in df.columns 
                      if c.endswith('_'+str(n))
                      and 'BLANK' not in c]
                    for n in range(1,51)}
# central_table

# Locate columns for claim form items 50-53, 58-62, 65 manually.
# These should be 'any but not all' indicators, but under a different claim form to column mapping.
# Also it's not clear what that mapping should be (ie which of these columns are ok to be null.)

bottom_table = {'50A': [c for c in df.columns if c.startswith(('50A','51A','52A','53A','58A','59A','60A','61A','62A','65A'))],
               '50B': [c for c in df.columns if c.startswith(('50B','51B','52B','53B','58B','59B','60B','61B','62B','65B'))],
               '50C': [c for c in df.columns if c.startswith(('50C','51C','52C','53C','58C','59C','60C','61C','62C','65C'))]
               }

# bottom_table

# Combine these into one dictionary.
groups_any_not_all.update(central_table)
groups_any_not_all.update(bottom_table)

output += """For-any-not-all group level indicators will be made for the following columns, 
			: \n %r \n\n\n""" %  groups_any_not_all

# Make the 'for all' indicator.
# This indicator is 1 iff all columns in group are null.
print "Making the for-all indicators."
for k, v in groups_all.items():
    name = 'MISS_GROUP_ALL_' + str(k)
    df[name] = (df[v].isnull().apply(sum,axis=1) == len(v)).astype(int)
    
# Make the 'for any' indicator.
# This indicator is 1 iff any columns in group are null.
print "Making the for-any indicators."
for k, v in groups_any.items():
    name = 'MISS_GROUP_ANY_' + str(k)
    df[name] = (df[v].isnull().apply(sum,axis=1) > 0).astype(int)
    
# Make the 'for any but not all' indicator.
# This indicator is 1 iff any *but not all* columns in group are null.
print "Making the for-any-not-all indicators."
for k, v in groups_any_not_all.items():
    name = 'MISS_GROUP_ANY_NOT_ALL_' + str(k)
    df[name] = (df[v].isnull().apply(sum,axis=1) > 0).astype(int) - (df[v].isnull().apply(sum,axis=1) == len(v)).astype(int)
    
# Make the indicators for individual columns.
# No indicator for columns that are all null anyway, but save these in a list.
null_cols = []

print "Making the individual indicators."
for k,v in indiv.items():
    if sum(df[v[0]].notnull()) == 0:
        null_cols.append(v)
    else:    
        df['MISS_' + str(k)] = (df[v[0]].isnull()).astype(int)
        
output += """These columns were uniformly null: \n %r \n\n\n""" %   null_cols

# Make count for diagnosis codes in claim form items 18-28.
print "Making a count."
df['COUNT_18_28'] = (df[claim_to_col[18]].notnull()).apply(sum,axis=1)

# Make count of at-least-partially-complete rows in central table of claim form.
# keys are 42_1 to 42_50
print "Making a count."
# first make column for each row of central table. column is 1 iff that row has a non-null entry.
for n in range(1,51):
    df['temp'+str(n)] = (df[groups_any_not_all['42_'+str(n)]].notnull()).apply(sum,axis=1) > 0
    
cols_to_sum = [c for c in df.columns if c.startswith('temp')]

# then sum those columns
df['COUNT_42_48'] = (df[cols_to_sum]).apply(sum,axis=1)

# then remove the temporary columns
for c in cols_to_sum:
    del df[c]
    

# Make count of claim form item 67, all letters.
print "Making a count."
df['COUNT_67'] = (df[claim_to_col[67]].notnull()).apply(sum,axis=1)

# Make a count of claim form item 74 (procedure codes).
print "Making a count."
proc_codes = [c for c in claim_to_col[74] if c.endswith('CODE')]
df['COUNT_74'] = (df[proc_codes].notnull()).apply(sum,axis=1)

# Make a column for ratio of count of diagnosis codes over count of procedure codes.
print "Making a ratio."
df['RATIO_DIAG_PROC'] = np.true_divide(df['COUNT_67'], df['COUNT_74'])
print "Fixing infs to NaNs."
def inf_to_nan(x):
    if np.isinf(x):
        return None
    else:
        return x
df['RATIO_DIAG_PROC'] = df['RATIO_DIAG_PROC'].apply(inf_to_nan)

# Make column for difference of stmt to and stmt from.
print "Making the difference of statement from and statement to dates."
temp = pd.to_datetime(df['6 STMT TO'], 
						dayfirst=True) - pd.to_datetime(df['6 STMT FRM'], 
													dayfirst=True)
df['DIFF_STMT_FROM_TO'] = np.true_divide(temp.astype(int), 86400000000000)

# Make indicator for STMT FRM date being different from ADMIT DATE
print "Making an indicator for statement from and admit date inequality."
df['DIFF_ADMIT_STMT'] = (df['6 STMT FRM'] == df['12 ADM DATE']).astype(int)


# Write the resulting dataframe to file.

# to run locally with full file
# df.to_csv("/Users/Allison.Gilmore/Documents/mercy/data/MISS HB UB CLAIM DATA.csv",sep='|')
# with open('mercy_missingness_output.txt','w') as f:
# 	f.write(output)

# to run on amber
df.to_csv("/home/ayasdi/jeanezra/mercy/cleanedlines/miss_ub_correct.dsv",sep='|')
with open('mercy_missingness_output.txt','w') as f:
	f.write(output)
	
# local test run
# df.to_csv("/Users/Allison.Gilmore/Documents/mercy/data/mercy_small_out.dsv",
# 			sep='|',
# 			index=False)

with open('mercy_missingness_output.txt','w') as f:
	f.write(output)